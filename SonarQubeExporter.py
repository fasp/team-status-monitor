'''
@author: Fabian Springer
Released under the MIT licence:
Free to modify, distribute, sublicence, private and commercial use
Author cannot be held liable
Mention of the original author, copyright and licence must be included
'''

from collections import OrderedDict
import requests
import json
# properties, required for issue retrieval. May contain sensitive data.
import MonitorSettings

'''
Collects issues from Sonarqube
'''
class Collector():

    def get_issues_for_assignees(self, assignees, project):
        """Fetches issues from Sonarqube for all assignees in the given list"""
        print('Retrieveing issues for assignees...')
        total_issues=0
        issues_by_assignee=OrderedDict()
        for assignee in assignees:
            relevantIssues=self.get_issues_for_assignee(assignee, project)
            total_issues+=len(relevantIssues)
            issues_by_assignee[assignee]=relevantIssues
        issues_by_assignee['TOTAL']=total_issues*[None]
        print('\tDone!')
        return issues_by_assignee

    def get_issues_for_assignee(self, assignee, project):
        relevant_issues=[]
        p=1
        r = requests.get(MonitorSettings.SONAR_URL+'/api/issues/search?assignees='+assignee+'&pageIndex='+str(p))
        issues=json.loads(r.content)['issues']
        while issues:  # do a paged search
            temp_rel_issues = self.__filter_open_issues_on_trunk(issues, project)
            relevant_issues+=temp_rel_issues
            p+=1  # why's there no simple increment ++?
            r = requests.get(MonitorSettings.SONAR_URL+'/api/issues/search?assignees='+assignee+'&pageIndex='+str(p))
            issues=json.loads(r.content)['issues']
        #print assignee+': '+str(len(relevant_issues))
        return relevant_issues

    def __filter_open_issues_on_trunk(self, issues, project):
        # more filters like and i[u'severity']!=u'MINOR'
        return list(filter(lambda i: i[u'project']==project and i[u'status']==u'OPEN', issues))

    def __init__(self, params):
        '''
        Constructor
        '''
