'''
@author: Fabian Springer
Released under the MIT licence:
Free to modify, distribute, sublicence, private and commercial use
Author cannot be held liable
Mention of the original author, copyright and licence must be included
'''
import pandas as pd

TABLE_BEGIN='\n\t<table class=\"table table-bordered table-hover\" font size=\"26\"  style=\"text-align: right\">\n'
TABLE_END='\n\t</table>\n'


class SonarHtmlManager():


    BY_ASSIGNEE = 0
    BY_DATE = 1

    def write_to_html_file(self, dst_file, history_csv, aliases, alignment=BY_ASSIGNEE):
        imports='<link rel=\"stylesheet\" href=\"lib/bootstrap-4.0.0/css/bootstrap.min.css\">\n <script src=\"lib/jquery-2.2.4.min.js\"></script>\n <script src=\"lib/bootstrap-3.3.6/dist/js/bootstrap.min.js\"></script>'
        HEADER = "<!DOCTYPE html>\n <html lang=\"en\">\n <head>\n <title>QA Issues (Trunk)</title>\n <meta charset=\"utf-8\">\n <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n "+imports+"\n</head>\n"
        html_file = open(dst_file, 'wb')
        html_string = HEADER
        html_string+='<body>\n'
        html_string+='<div style=\"padding: 10px 50px 0 50px\"><div class=\"row\"><div class=\"col-sm\"><img src=\"Sonarqube.png\" height=80%></div><div class=\"col-sm\"><h1>Issues@trunk</h1><h3>Questions: Fabian Springer</h3></div></div></div>\n'
        html_string+='<div style="font-size:46px">'
        html_string+=TABLE_BEGIN
        if alignment==self.BY_DATE:
            history_csv=history_csv.transpose()
        html_string+=self.__create_html_table(history_csv, alignment, aliases)
        html_string+=TABLE_END
        html_string+=__create_lunch_ticker()
        html_string+='</div>'
        html_string+='</body>\n</html>'
        html_file.write(html_string)
        html_file.close()
        print("Success!")
        print("Produced file " + dst_file)

def __create_lunch_ticker():
    with open('lunchTickerSnippet.txt') as f:
        lunch_ticker_code=f.read()
    f.close()
    return lunch_ticker_code

    def __create_html_table(self, history_csv, alignment, aliases):
        header=history_csv.iloc[0]  # first row
        history_csv=history_csv.tail(3)
        headerList=list(history_csv)
        html_table=''
        if alignment==self.BY_ASSIGNEE:
            for row in history_csv.itertuples(index=True, name='Pandas'):
                html_table='<tr>'+''.join('<td><b>%s</b></td>' % h for h in headerList)+'</tr>'
                html_table+=self.__create_html_table_row_by_assignee(row, aliases)
        elif alignment==self.BY_DATE:
            header=filter(lambda ass: pd.notnull(ass), header)
            alias_header=['']+map(lambda ass: aliases[ass], header)
            html_table=2*'\t'+'<tr>'+''.join('<td><b>%s</b></td>' % a for a in alias_header)+'</tr>'
            #n_cols=len(history_csv.index)
            n_rows=len(history_csv)
            dates=history_csv.axes[0]
            for row_idx in range(0,n_rows):
                row=history_csv.iloc[row_idx]
                complete_row=[dates[row_idx]]+list(row)
                html_table+='\n'+2*'\t'+self.__create_html_table_row_by_date(complete_row)
        return html_table

    def __create_html_table_row_by_assignee(self, row, aliases):
        table_row='<tr>\n'
        # 0 := idx; 1 := assignee; 2:n-1 := issues
        table_row+='<td><b>'+aliases[row[1]]+'</b></td>'
        prev_iss=int(row[2])
        for issue in range(2, len(row)):
            curr_iss=int(row[issue])
            if prev_iss<curr_iss:
                table_row+='\t<td class="danger">'
            elif prev_iss>curr_iss:
                table_row+='\t<td class="success">'
            else:
                table_row+='\t<td style="background: white;">'
            table_row+=str(curr_iss)
            prev_iss=curr_iss
        table_row+='<td></td>\n</tr>\n'
        return table_row

    def __create_html_table_row_by_date(self, row):
        table_row='<tr>'+'<td><b>%s</b></td>'%row[0]  # the date
        table_row+=''.join('<td>%s</td>' % h for h in row[1:-1])  # the issues
        table_row+='<td><b>'+str(row[-1])+'</b></td></tr>'  # the total value
        return table_row

    def __init__(self, params):
        '''
        Constructor
        '''
