# File listing all assignees:
ASSIGNEE_LIST="path/to/dir/Team-Status-Monitor/assignees.txt"
# sonarqube url, something like:
SONAR_URL='http://sonar.int.company:8080'
# sonarqube project id
PROJECT_TRUNK=u'com.company:ourProject:trunk'
# Internal project ID trunk on SonarQube
TRUNK_ID='263b18cc-8731-4644-ab16-ab337ca60050'
# Team administror, full email format, e.g. external.name.lastname@domain.com
SONARQUBE_ADMIN=''
# Team lead, full email format, e.g. external.name.lastname@domain.com
TEAM_LEAD=''
# domain
DOMAIN='domain.com'
# Full path of the existing and ever-expanding history.csv table file:
HISTORY_LOCATION="path/to/dir/Team-Status-Monitor/history.csv"
# Full path of the generated history.html output
GENERATED_HTML_LOCATION="path/to/dir/Team-Status-Monitor/history.html"
# Target path at the (remote) monitor, e.g. in the format: username@ip:/output/folder/path
HISTORY_TARGET_LOCATION="username@ip:/output/folder/path"
# ssh password for transmission to (remote) monitor
PI_PASSWORD="veryStrongPassword"