#pylint: disable=invalid-name

'''
@author: Fabian Springer
Released under the MIT licence:
Free to modify, distribute, sublicence, private and commercial use
Author cannot be held liable
Mention of the original author, copyright and licence must be included
'''

import datetime
import os
from shutil import copyfile
import pandas as pd

def expand_csv_history(file_name, issues_by_assignees):
    ''' Recognizes an existing csv history and expands it by the newly found issues'''

    # TODO make this decision (new file or expand existing) in a caller method
    if os.path.exists(file_name):
        backup_file=file_name.split('.')[0]+'_backup.csv'
        copyfile(file_name, backup_file)
        print("Created "+backup_file)
        print("Adding issues to "+file_name+"...")
        history=pd.read_csv(file_name, sep=';')
    else:
        with open(file_name, 'w') as file_handle:
            file_handle.write('')
        history=pd.DataFrame({"Assignee":issues_by_assignees.keys()})

    now=datetime.datetime.now()
    current_date_string=now.strftime("%d. %b")
    found_issues=issues_by_assignees.values()
    new_col=pd.DataFrame({current_date_string:found_issues})
    result = pd.concat([history, new_col], axis=1)
    result.to_csv(file_name, sep=';', index=False)
    print("Done!")
    return result
