'''
@author: Fabian Springer
Released under the MIT licence:
Free to modify, distribute, sublicence, private and commercial use
Author cannot be held liable.
Mention of the original author, copyright and licence must be included
'''

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.utils.datastructures import MultiValueDict
from Monitor_Settings import * #pylint: disable=unused-wildcard-import

def send_all_mails(current_issues):
    total='TOTAL'
    for ass_and_iss in current_issues:
        assignee=ass_and_iss
        if assignee is not total:
            message_text = __build_message_with_assignees_issues(assignee,
                                                                len(current_issues[assignee]))
            to_addr=[assignee+'@'+DOMAIN]
            cc=[SONARQUBE_ADMIN+'@'+DOMAIN,
                TEAM_LEAD+'@'+DOMAIN]
            bcc=[]
            from_address='donotreply@'+DOMAIN
            __send_mail(to_addr, cc, bcc, from_address, message_text)


def __build_message_with_assignees_issues(assignee, relevant_issues):
    assignee_url=SONAR_URL+'/issues?projectUuids='+TRUNK_ID+'&assignees='+assignee
    issues_by_rule = MultiValueDict()
    for issue in relevant_issues:
        issues_by_rule.appendlist(issue['rule'], issue)
    message_text='\n\n<h1>'+assignee+'</h1>\n'
    message_text+=(str(len(relevant_issues))+' issues in total, '+'\t'
                   +__url_to_link(assignee_url, 'see all issues for assignee')+'<br>\n')
    message_text+='\tViolated rules in total: '+str(len(issues_by_rule))+'<br>\n'
    message_text+='<ul>'
    for rule in issues_by_rule.keys():
        message_text+='<li>'
        issue_rule=issues_by_rule.getlist(rule)
        rule_url=assignee_url+'&rules='+rule
        message_text+=('\t'+__url_to_link(rule_url, rule)+' '+
                       str(len(issue_rule))+ ' violation(s)''<br>\n')
        message_text+='</li>'
    message_text+='</ul>'
    return message_text

def __url_to_link(url, display_text):
    return '<a href="'+url+'">'+display_text+'</a>'

def __send_mail(recipients, copies, bcc, from_address, message_text):
    """Sends a mail using the spcified params. Static.

    Keyword arguments:
    recipients         -- The main recipient
    copies             -- Additional recipient (e.g. team lead)
    bcc            -- blind carbon copy
    from_address     -- Sender (noReply@KN)
    message_text    -- The message text
    """
    email = MIMEMultipart('alternative')
    email['Subject'] = "Weekly SonarQube Issue Report"
    email['From'] = from_address
    email['To'] = ','.join(recipients)
    email['Cc'] = ','.join(copies)
    email['Bcc'] = ','.join(bcc)

    message_body=MIMEText(message_text, 'html')
    email.attach(message_body)

    server = smtplib.SMTP('smtp-citc.int.kn')
    server.set_debuglevel(1)
    all_tos=recipients+copies+bcc
    server.sendmail(from_address, all_tos, email.as_string())
    server.quit()
    print()
