#pylint: disable=invalid-name
'''
@author: Fabian Springer
Released under the MIT licence:
Free to modify, distribute, sublicence, private and commercial use
Author cannot be held liable.
Mention of the original author, copyright and licence must be included
'''

import subprocess
from collections import OrderedDict

# properties, required for issue retrieval. May contain sensitive data.
from Monitor_Settings import * #pylint: disable=unused-wildcard-import
import Mailer
import SonarQubeExporter
import SonarCsvManager
from SonarHtmlManager import SonarHtmlManager

def __get_assignees(filename):
    with open(filename) as f:
        assignee_list=filter(lambda l: not l.startswith('#'), [line.strip() for line in f])
        assignees = OrderedDict((l[0],l[1]) for l in (l.split(':') for l in assignee_list))
    assignees['TOTAL']='TOTAL'
    return assignees

def __transfer_history_to_pi():
    copy_to_pi_command = ('pscp -scp -pw '+PI_PASSWORD+' '+
        GENERATED_HTML_LOCATION+' '+HISTORY_TARGET_LOCATION)
    copy_status=subprocess.call(copy_to_pi_command)
    print(copy_status)

if __name__ == '__main__':
    #pylint: disable=invalid-name
    ASSIGNEE_ALIASES = __get_assignees(ASSIGNEE_LIST)
    sqe=SonarQubeExporter.Collector(None)
    current_issues=sqe.get_issues_for_assignees(ASSIGNEE_ALIASES.keys(),
                                                PROJECT_TRUNK)

    Mailer.send_all_mails(current_issues)

    current_issue_counts=OrderedDict([(ci,len(current_issues[ci])) for ci in current_issues])
    history_csv=SonarCsvManager.expand_csv_history(HISTORY_LOCATION, current_issues)

    html_man=SonarHtmlManager(None)
    html_man.write_to_html_file(GENERATED_HTML_LOCATION, history_csv,
                                ASSIGNEE_ALIASES, alignment=html_man.BY_DATE)

    __transfer_history_to_pi()
